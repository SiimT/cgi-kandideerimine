package com.cgi.dentistapp.service;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@WebServlet(  name = "DentistVisitServlet",
        description = "Example Servlet Using Annotations",
        urlPatterns = {"/DentistVisitServlet"})
public class DentistVisitServlet extends HttpServlet {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:~/test";

    //  Database credentials
    static final String USER = "sa";
    static final String PASS = "";
    static ResultSet rs;
    static StringBuilder sb;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();
            String sql = "SELECT * FROM DENTIST_VISIT;";
            rs = stmt.executeQuery(sql);
            sb = new StringBuilder();
            sb.append("{"+"\"data\": [");
            while(rs.next()) {
                // Retrieve by column name
                int id = rs.getInt("id");
                String dentistName = rs.getString("name");
                String visitTime = rs.getTimestamp("visitTime").toString();
                System.out.println("Visit time:" +visitTime);
                sb.append("["+"\""+Integer.toString(id)+"\",\""+dentistName+"\","+"\""+visitTime+"\"],");
            }
            sb.deleteCharAt(sb.length()-1);
            stmt.close();
            conn.close();
            sb.append("]}");
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } //end finally try
        } //end try
        if (rs != null)
        {
            System.out.println("Servlet:"+sb.toString());
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(sb.toString());
        }
        else
        {
            //TODO Vaja asendada tühja JSON'iga
            response.getWriter().write("");
        }


    }
}