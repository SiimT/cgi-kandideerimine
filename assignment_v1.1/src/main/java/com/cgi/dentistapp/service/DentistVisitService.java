package com.cgi.dentistapp.service;

import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.transaction.Transactional;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@Service
@Transactional
public class DentistVisitService {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:~/test";

    //  Database credentials
    static final String USER = "sa";
    static final String PASS = "";


    public static void createVisitDatabase() //https://www.tutorialspoint.com/h2_database/h2_database_jdbc_connection.htm
    {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            //System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //System.out.println("Creating table in given database...");
            stmt = conn.createStatement();
            String sql = "CREATE TABLE DENTIST_VISIT " +
                    "(id INTEGER not NULL AUTO_INCREMENT, " +
                    " name VARCHAR(255), " +
                    " visitTime DATETIME, " +
                    " PRIMARY KEY ( id ))";
            stmt.executeUpdate(sql);
            //System.out.println("Created table in given database...");
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } //end finally try
        } //end try
        //System.out.println("Goodbye!");
    }

    public void addVisit(String dentistName, Date visitTime)
    {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //Cannot conver TIMESTAMP constant fix:
            DateFormat converter = new SimpleDateFormat(" yyyy-MM-dd hh:mm:ss");
            //converter.setTimeZone(TimeZone.getTimeZone(TimeZone.getDefault().toString()));
            String sqlDate = converter.format(visitTime);
            System.out.println("Time before insertion:"+sqlDate);
            stmt = conn.createStatement();
            String sql = "INSERT INTO DENTIST_VISIT (NAME,VISITTIME) VALUES ('"
                    +dentistName+"','"
                    +sqlDate
                    +"');";
            stmt.executeUpdate(sql);
            //System.out.println("Created table in given database...");
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            //finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } //end finally try
        } //end try
    }
}

