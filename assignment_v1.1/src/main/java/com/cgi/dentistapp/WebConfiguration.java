package com.cgi.dentistapp;

import com.cgi.dentistapp.service.DentistVisitServlet;
import org.h2.server.web.WebServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfiguration {

    @Bean
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }
    @Bean
    ServletRegistrationBean databaseServletRegistration() {
        ServletRegistrationBean databaseServletBean = new ServletRegistrationBean(new DentistVisitServlet());
        databaseServletBean.addUrlMappings("/DentistVisitServlet");
        databaseServletBean.setLoadOnStartup(1);
        return databaseServletBean;
    }
}
