package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DentistVisitDTO {

    @Size(min = 1, max = 50)
    String dentistName;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    Date visitTime;

    public DentistVisitDTO() {
    }

    public DentistVisitDTO(String dentistName, Date visitTime) {
        this.dentistName = dentistName;
        this.visitTime = visitTime;
    }

    public String getDentistName() {
        return dentistName;
    }

    public void setDentistName(String dentistName) {
        this.dentistName = dentistName;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        Date time = null;
        Date firstVisit = null;
        Date lastVisit = null;
        try
        {
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        firstVisit = parser.parse("8:30");                            // Esimene aeg, mil on võimalik visiidile registreeruda
        lastVisit = parser.parse("16:30");                      // Viimane aeg, mil on võimalik visiidile registreeruda
        String dateTime = visitTime.toString();
            System.out.println(dateTime);
        time= new SimpleDateFormat("HH:mm").parse(dateTime.substring(11,17));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        if (time != null)
        {
            if(time.before(firstVisit) || time.after(lastVisit))
            {
                //TODO Oleks vaja mingi reaalne exception ja error message ka kasutajale visata
                System.out.println("Forbidden time.");
            }
            else
            {
                this.visitTime = visitTime;
            }
        }


    }
}
