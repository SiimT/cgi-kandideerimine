package com.cgi.dentistapp;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Locale;

@SpringBootApplication
public class DentistAppApplication {

    public static void main(String[] args) {
        //TODO H2 db jääb mällu alles, iga kord ära kustutada?
        DentistVisitService.createVisitDatabase(); // Esmakordsel käivitamisel loome tabeli visiitide hoidmiseks.
        SpringApplication.run(DentistAppApplication.class, args);
    }
}
